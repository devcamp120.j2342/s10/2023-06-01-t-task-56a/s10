package com.devcamp.demorestapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin
public class DemoController {
    @GetMapping("/")
    public String helloworld() {
        return "Xin chào Devcamp";
    }

    @GetMapping("/rainbow")
    public ArrayList<String> getRainbow() {
        ArrayList<String> rainbow = new ArrayList<>();

        rainbow.add("red");
        rainbow.add("orange");
        rainbow.add("yellow");
        rainbow.add("green");
        rainbow.add("blue");
        rainbow.add("indigo");
        rainbow.add("violet");

        return rainbow;
    }

    @GetMapping("/split")
    public ArrayList<String> splitString(@RequestParam String inputString) {
        ArrayList<String> result = new ArrayList<>();

        String[] split_array = inputString.split(" ");

        for (String string : split_array) {
            result.add(string);
        }

        return result;
    }
}
